package com.example.demo_spring.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActuatorController {

  @GetMapping(value = "/ping")
  public ResponseEntity<String> ping() {
    return new ResponseEntity<>("pong", HttpStatus.OK);
  }
  @GetMapping(value = "/sum")
  public ResponseEntity<String> cal() {
    return new ResponseEntity<>("2+2=4", HttpStatus.OK);
  }
}
